package com.egosventures.car360sample.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.egosventures.stable360.display.model.OnlineViewerConfiguration;


public class PreferencesHelper {

    // ---------------------------------------------------------------------------------------------
    // region Private attributes

    // OnlineViewer configuration:
    private static String ONLINE_VIEWER_CONFIGURATION_CAR_ID = "ONLINE_VIEWER_CONFIGURATION_CAR_ID";
    private static String ONLINE_VIEWER_CONFIGURATION_CUSTOM_ROOT_URL = "ONLINE_VIEWER_CONFIGURATION_CUSTOM_ROOT_URL";
    private static String ONLINE_VIEWER_CONFIGURATION_RESOLUTION = "ONLINE_VIEWER_CONFIGURATION_RESOLUTION";
    private static String ONLINE_VIEWER_CONFIGURATION_SHOW_FIRST_IMAGE_BEFORE_STARTING_LOADING = "ONLINE_VIEWER_CONFIGURATION_SHOW_FIRST_IMAGE_BEFORE_STARTING_LOADING";
    private static String ONLINE_VIEWER_CONFIGURATION_FIRST_IMAGE_RESOLUTION = "ONLINE_VIEWER_CONFIGURATION_FIRST_IMAGE_RESOLUTION";
    private static String ONLINE_VIEWER_CONFIGURATION_FIRST_IMAGE_ANIMATION_DURATION = "ONLINE_VIEWER_CONFIGURATION_FIRST_IMAGE_ANIMATION_DURATION";
    private static String ONLINE_VIEWER_CONFIGURATION_API_URL = "ONLINE_VIEWER_CONFIGURATION_API_URL";
    private static String ONLINE_VIEWER_CONFIGURATION_JPEG_QUALITY = "ONLINE_VIEWER_CONFIGURATION_JPEG_QUALITY";
    private static String ONLINE_VIEWER_CONFIGURATION_BACKGROUND_COLOR = "ONLINE_VIEWER_CONFIGURATION_BACKGROUND_COLOR";
    private static String ONLINE_VIEWER_CONFIGURATION_PRELOAD_TAGS_MEDIA = "ONLINE_VIEWER_CONFIGURATION_PRELOAD_TAGS_MEDIA";

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Get shared prefs and editor

    private static SharedPreferences getSharedPreferences(Context context) { return PreferenceManager.getDefaultSharedPreferences(context); }
    private static SharedPreferences.Editor getEditor(Context context) { return getSharedPreferences(context).edit(); }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region OnlineViewerConfiguration

    public static OnlineViewerConfiguration getOnlineViewerConfiguration(Context context) {

        SharedPreferences prefs = getSharedPreferences(context);

        int color = prefs.getInt(ONLINE_VIEWER_CONFIGURATION_BACKGROUND_COLOR, -1);

        OnlineViewerConfiguration configuration = new OnlineViewerConfiguration();
        configuration.carId = prefs.getString(ONLINE_VIEWER_CONFIGURATION_CAR_ID, null);
        configuration.customRootUrl = prefs.getString(ONLINE_VIEWER_CONFIGURATION_CUSTOM_ROOT_URL, null);
        configuration.resolution = getSupportedResolution(prefs, ONLINE_VIEWER_CONFIGURATION_RESOLUTION, OnlineViewerConfiguration.SupportedResolution.RESOLUTION_960_540);
        configuration.showFirstImageBeforeStartingLoading = prefs.getBoolean(ONLINE_VIEWER_CONFIGURATION_SHOW_FIRST_IMAGE_BEFORE_STARTING_LOADING, true);
        configuration.firstImageResolution = getSupportedResolution(prefs, ONLINE_VIEWER_CONFIGURATION_FIRST_IMAGE_RESOLUTION, null);
        configuration.firstImageAnimationDuration = prefs.getInt(ONLINE_VIEWER_CONFIGURATION_FIRST_IMAGE_ANIMATION_DURATION, 3000);
        configuration.apiUrl = prefs.getString(ONLINE_VIEWER_CONFIGURATION_API_URL, OnlineViewerConfiguration.API_URL);
        configuration.jpegQuality = prefs.getInt(ONLINE_VIEWER_CONFIGURATION_JPEG_QUALITY, 5);
        configuration.backgroundColor = color == -1 ? null : color;
        configuration.preLoadTagsMedia = prefs.getBoolean(ONLINE_VIEWER_CONFIGURATION_PRELOAD_TAGS_MEDIA, true);

        return configuration;
    }

    public static void setOnlineViewerConfiguration(Context context, OnlineViewerConfiguration configuration) {

        SharedPreferences.Editor editor = getEditor(context);
        editor.putString(ONLINE_VIEWER_CONFIGURATION_CAR_ID, configuration.carId);
        editor.putString(ONLINE_VIEWER_CONFIGURATION_CUSTOM_ROOT_URL, configuration.customRootUrl);
        editor.putString(ONLINE_VIEWER_CONFIGURATION_RESOLUTION, configuration.resolution.name());
        editor.putBoolean(ONLINE_VIEWER_CONFIGURATION_SHOW_FIRST_IMAGE_BEFORE_STARTING_LOADING, configuration.showFirstImageBeforeStartingLoading);
        editor.putString(ONLINE_VIEWER_CONFIGURATION_FIRST_IMAGE_RESOLUTION, configuration.firstImageResolution == null ? null : configuration.firstImageResolution.name());
        editor.putInt(ONLINE_VIEWER_CONFIGURATION_FIRST_IMAGE_ANIMATION_DURATION, configuration.firstImageAnimationDuration);
        editor.putString(ONLINE_VIEWER_CONFIGURATION_API_URL, configuration.apiUrl);
        editor.putInt(ONLINE_VIEWER_CONFIGURATION_JPEG_QUALITY, configuration.jpegQuality);
        editor.putInt(ONLINE_VIEWER_CONFIGURATION_BACKGROUND_COLOR, configuration.backgroundColor == null ? -1 : configuration.backgroundColor);
        editor.putBoolean(ONLINE_VIEWER_CONFIGURATION_PRELOAD_TAGS_MEDIA, configuration.preLoadTagsMedia);
        editor.commit();
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Utils

    private static OnlineViewerConfiguration.SupportedResolution getSupportedResolution(SharedPreferences prefs, String key, OnlineViewerConfiguration.SupportedResolution defaultValue) {

        String stringValue = prefs.getString(key, defaultValue == null ? null : defaultValue.name());
        return stringValue == null ? null : OnlineViewerConfiguration.SupportedResolution.valueOf(stringValue);
    }

    // endregion
    // ---------------------------------------------------------------------------------------------
}
