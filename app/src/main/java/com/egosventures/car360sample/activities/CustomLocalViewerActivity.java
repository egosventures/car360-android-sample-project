package com.egosventures.car360sample.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.egosventures.car360sample.R;
import com.egosventures.stable360.capture.activities.Add3dTagActivity;
import com.egosventures.stable360.capture.activities.Add3dTagConfirmationActivity;
import com.egosventures.stable360.capture.fragments.LocalViewerFragment;
import com.egosventures.stable360.core.Stable360Framework;
import com.egosventures.stable360.core.activities.InsideCaptureSourceChooserActivity;
import com.egosventures.stable360.core.activities.InsideViewerActivity;
import com.egosventures.stable360.core.model.Tag3d;
import com.egosventures.stable360.core.utils.IconPosition;


public class CustomLocalViewerActivity extends AppCompatActivity implements LocalViewerFragment.LocalViewerFragmentLoadingListener {

    // ---------------------------------------------------------------------------------------------
    // region Public attributes

    public final static String TAG = "ViewerActivity";
    public final static String EXTRA_SPIN_ID = "EXTRA_SPIN_ID";

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Private attributes

    private final static int REQUEST_INSIDE_VIEWER = 0;
    private final static int REQUEST_ACTIVITY_ADD_3D_TAG = 1;

    private LocalViewerFragment mViewer;
    private Button mEnterTagsEditionModeButton;
    private Button mExitTagsEditionModeButton;
    private Button mAddTagButton;

    private BroadcastReceiver fragmentMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            int event = intent.getIntExtra(LocalViewerFragment.EXTRA_FRAGMENT_EVENT, -1);

            switch (event) {

                case LocalViewerFragment.EVENT_NULL_USER: {

                    Log.e(TAG, "EVENT_NULL_USER");
                    break;
                }

                case LocalViewerFragment.EVENT_FRAMEWORK_INIT_ERROR: {

                    // Get the error:
                    Stable360Framework.Stable360FrameworkError error = (Stable360Framework.Stable360FrameworkError) intent.getSerializableExtra(LocalViewerFragment.EXTRA_FRAMEWORK_INIT_ERROR);
                    Log.e(TAG, "EVENT_FRAMEWORK_INIT_ERROR: " + error.name());
                    break;
                }
            }
        }
    };

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Lifecycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate layout:
        setContentView(R.layout.activity_custom_local_viewer);

        // Enable up navigation:
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Get the widgets:
        mEnterTagsEditionModeButton = (Button) findViewById(R.id.enterTagsEditionModeButton);
        mExitTagsEditionModeButton = (Button) findViewById(R.id.exitTagsEditionModeButton);
        mAddTagButton = (Button) findViewById(R.id.addTagButton);

        // Load the spin:
        if (getIntent() != null && getIntent().getStringExtra(EXTRA_SPIN_ID) != null) {

            // Add the viewer:
            if (savedInstanceState == null) {

                String spinId = getIntent().getStringExtra(EXTRA_SPIN_ID);

                mViewer = new LocalViewerFragment();
                Bundle bundle = new Bundle();

                // The spin id is a mandatory extra:
                bundle.putString(LocalViewerFragment.EXTRA_SPIN_ID, spinId);

                // You can customize the background color:
                bundle.putSerializable(LocalViewerFragment.EXTRA_BACKGROUND_COLOR, Color.RED);

                // You can customize the message displayed if the framework initialisation led to a FRAMEWORK_INIT_ERROR event:
                bundle.putBoolean(LocalViewerFragment.EXTRA_SHOW_FRAMEWORK_INIT_ERROR_MESSAGE, true);
                bundle.putString(LocalViewerFragment.EXTRA_FRAMEWORK_INIT_ERROR_MESSAGE, "Custom framework init error message");
                bundle.putInt(LocalViewerFragment.EXTRA_FRAMEWORK_INIT_ERROR_ICON, R.drawable.ic_action_warning_light);
                bundle.putSerializable(LocalViewerFragment.EXTRA_FRAMEWORK_INIT_ERROR_ICON_POSITION, IconPosition.RIGHT);

                // You can customize the message displayed if the framework initialisation led to a NULL_USER event:
                bundle.putBoolean(LocalViewerFragment.EXTRA_SHOW_NULL_USER_MESSAGE, true);
                bundle.putString(LocalViewerFragment.EXTRA_NULL_USER_MESSAGE, "Custom null user message");
                bundle.putInt(LocalViewerFragment.EXTRA_NULL_USER_ICON, R.drawable.ic_action_warning_light);
                bundle.putSerializable(LocalViewerFragment.EXTRA_NULL_USER_ICON_POSITION, IconPosition.BOTTOM);

                mViewer.setArguments(bundle);

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.customLocalViewerFrameLayout, mViewer, "VIEWER").commit();
            } else {

                mViewer = (LocalViewerFragment) getSupportFragmentManager().findFragmentByTag("VIEWER");
            }

            mViewer.setLoadingListener(this);
        }

        // Listen clicks:
        findViewById(R.id.openInsideViewerButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onOpenInsideViewerButtonClick();
            }
        });

        mEnterTagsEditionModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onEnterTagsEditionModeButtonClick();
            }
        });

        mExitTagsEditionModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onExitTagsEditionModeButtonClick();
            }
        });

        mAddTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onAddTagButtonClick();
            }
        });
    }

    @Override
    protected void onResume() {

        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(fragmentMessageReceiver, new IntentFilter(LocalViewerFragment.BROADCAST_FRAGMENT_MESSAGE));
    }

    @Override
    protected void onPause() {

        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fragmentMessageReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case REQUEST_INSIDE_VIEWER: {

                mViewer.reloadSpinFromMetadata();
                break;
            }

            case REQUEST_ACTIVITY_ADD_3D_TAG: {

                switch (resultCode) {

                    case Add3dTagActivity.RESULT_SPIN_NOT_FOUND:
                    case Add3dTagActivity.RESULT_UNKNOWN_SPIN_DEGREES:
                    case Add3dTagActivity.RESULT_BAD_GYRO_DATA:
                    case Add3dTagActivity.RESULT_SPIN_NOT_COMPUTED_IN_3D: {

                        Toast.makeText(this, "Sorry, an error occurred while creating the tag", Toast.LENGTH_LONG).show();

                        break;
                    }
                    case Activity.RESULT_OK: {

                        // The 3D tag creation succeeded, get the tag:
                        Tag3d tag3d = data.getParcelableExtra(Add3dTagConfirmationActivity.EXTRA_TAG);

                        mViewer.reloadSpinFromMetadata();
                        mViewer.setImage(tag3d.getCenterFrame());
                        mViewer.showTagViewForTag(tag3d, true, null);

                        break;
                    }
                }

                break;
            }
        }
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();

        if (id == android.R.id.home) { onBackPressed(); return true; }
        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region LocalViewerFragment.LocalViewerFragmentLoadingListener

    @Override
    public void onLoadingDone() {

        Log.i(TAG, "Local spin have successfully been loaded");
    }

    @Override
    public void onLoadingError(LocalViewerFragment.LoadingError loadingError) {

        Log.e(TAG, "An error occurred while loading the local spin: " + loadingError.name());
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region User interaction

    private void onOpenInsideViewerButtonClick() {

        if(mViewer.hasInsideCapture(this)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("What do you want to do?");
            builder.setItems(new CharSequence[] {
                    "Open the inside capture in the default viewer",
                    "Open the inside capture in the custom viewer",
                    "Open the inside capture in the simple custom viewer (with no tags edition support)",
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    switch (i) {

                        case 0: {
                            openDefaultInsideViewer();
                            break;
                        }
                        case 1: {
                            openCustomInsideViewer();
                            break;
                        }
                        case 2: {
                            openSimpleCustomInsideViewer();
                            break;
                        }
                    }
                }
            });
            builder.show();
        }
        else {

            Intent intent = new Intent(this, InsideCaptureSourceChooserActivity.class);
            intent.putExtra(InsideCaptureSourceChooserActivity.EXTRA_FILE_NAME, mViewer.getSpin().getId());
            intent.putExtra(InsideCaptureSourceChooserActivity.EXTRA_SPIN_FOLDER, mViewer.getSpin().getFolder(this));
            startActivity(intent);
        }
    }

    private void openDefaultInsideViewer() {

        if(mViewer.hasInsideCapture(this)) {

            Intent intent = new Intent(this, InsideViewerActivity.class);
            intent.putExtra(InsideViewerActivity.EXTRA_SPIN, mViewer.getSpin());
            intent.putExtra(InsideViewerActivity.EXTRA_CAN_START_TAGS_EDITION_MODE, true);
            intent.putExtra(InsideViewerActivity.EXTRA_CAN_RECAPTURE, true);
            startActivityForResult(intent, REQUEST_INSIDE_VIEWER);
        }
        else {

            Intent intent = new Intent(this, InsideCaptureSourceChooserActivity.class);
            intent.putExtra(InsideCaptureSourceChooserActivity.EXTRA_FILE_NAME, mViewer.getSpin().getId());
            intent.putExtra(InsideCaptureSourceChooserActivity.EXTRA_SPIN_FOLDER, mViewer.getSpin().getFolder(this));
            startActivity(intent);
        }
    }

    private void openCustomInsideViewer() {

        if(mViewer.hasInsideCapture(this)) {

            Intent intent = new Intent(this, CustomInsideViewerActivity.class);
            intent.putExtra(CustomInsideViewerActivity.EXTRA_SPIN, mViewer.getSpin());
            startActivityForResult(intent, REQUEST_INSIDE_VIEWER);
        }
        else {

            Intent intent = new Intent(this, InsideCaptureSourceChooserActivity.class);
            intent.putExtra(InsideCaptureSourceChooserActivity.EXTRA_FILE_NAME, mViewer.getSpin().getId());
            intent.putExtra(InsideCaptureSourceChooserActivity.EXTRA_SPIN_FOLDER, mViewer.getSpin().getFolder(this));
            startActivity(intent);
        }
    }

    private void openSimpleCustomInsideViewer() {

        if(mViewer.hasInsideCapture(this)) {

            Intent intent = new Intent(this, SimpleCustomInsideViewerActivity.class);
            intent.putExtra(SimpleCustomInsideViewerActivity.EXTRA_SPIN, mViewer.getSpin());
            startActivityForResult(intent, REQUEST_INSIDE_VIEWER);
        }
        else {

            Intent intent = new Intent(this, InsideCaptureSourceChooserActivity.class);
            intent.putExtra(InsideCaptureSourceChooserActivity.EXTRA_FILE_NAME, mViewer.getSpin().getId());
            intent.putExtra(InsideCaptureSourceChooserActivity.EXTRA_SPIN_FOLDER, mViewer.getSpin().getFolder(this));
            startActivity(intent);
        }
    }

    private void onEnterTagsEditionModeButtonClick() {

        mEnterTagsEditionModeButton.setVisibility(View.GONE);
        mExitTagsEditionModeButton.setVisibility(View.VISIBLE);
        mAddTagButton.setVisibility(View.VISIBLE);

        mViewer.setTagEditionModeActive(true);
    }

    private void onExitTagsEditionModeButtonClick() {

        mEnterTagsEditionModeButton.setVisibility(View.VISIBLE);
        mExitTagsEditionModeButton.setVisibility(View.GONE);
        mAddTagButton.setVisibility(View.GONE);

        mViewer.setTagEditionModeActive(false);
    }

    private void onAddTagButtonClick() {

        Intent intent = new Intent(this, Add3dTagActivity.class);
        intent.putExtra(Add3dTagActivity.EXTRA_SPIN_ID, mViewer.getSpin().getId());
        startActivityForResult(intent, REQUEST_ACTIVITY_ADD_3D_TAG);
    }

    // endregion
    // ---------------------------------------------------------------------------------------------
}
