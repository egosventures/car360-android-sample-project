package com.egosventures.car360sample.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.egosventures.car360sample.R;
import com.egosventures.stable360.core.fragments.InsideViewerFragment;
import com.egosventures.stable360.core.model.Spin;


public class SimpleCustomInsideViewerActivity extends AppCompatActivity {

    // ---------------------------------------------------------------------------------------------
    // region Public attributes

    /**
     * The spin's folder path (String, mandatory).
     */
    public final static String EXTRA_SPIN = "EXTRA_SPIN";

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Lifecycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate layout:
        setContentView(R.layout.activity_simple_custom_inside_viewer);

        // Enable up navigation:
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Add the viewer:
        if (savedInstanceState == null) {

            // Get the intent data:
            Spin spin = getIntent().getParcelableExtra(EXTRA_SPIN);

            Bundle bundle = new Bundle();
            bundle.putParcelable(InsideViewerFragment.EXTRA_SPIN, spin);

            InsideViewerFragment viewer = new InsideViewerFragment();
            viewer.setArguments(bundle);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.insideViewerContainer, viewer, "INSIDE_VIEWER").commit();
        }
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();

        if (id == android.R.id.home) { onBackPressed(); return true; }
        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    // ---------------------------------------------------------------------------------------------
}
