package com.egosventures.car360sample.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.egosventures.car360sample.R;
import com.egosventures.stable360.core.activities.InsideCaptureSourceChooserActivity;
import com.egosventures.stable360.core.activities.TagMediaActivity;
import com.egosventures.stable360.core.fragments.InsideViewerFragment;
import com.egosventures.stable360.core.model.Spin;
import com.egosventures.stable360.core.model.Tag;
import com.egosventures.stable360.core.utils.Log;
import com.egosventures.stable360.core.views.TagView;

/**
 * This activity can be used to display an inside capture. It can also be used to add / edit inside
 * tags.
 */
public class CustomInsideViewerActivity extends AppCompatActivity implements InsideViewerFragment.InsideViewerFragmentLoadingListener, TagView.TagViewListener, InsideViewerFragment.InsideViewerFragmentListener {

    // ---------------------------------------------------------------------------------------------
    // region Public attributes

    /**
     * The spin's folder path (String, mandatory).
     */
    public final static String EXTRA_SPIN = "EXTRA_SPIN";

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Private attributes

    private final static String STATE_IS_ENTER_TAGS_EDITION_MODE_BUTTON_CONTAINER_VISIBLE = "STATE_IS_ENTER_TAGS_EDITION_MODE_BUTTON_CONTAINER_VISIBLE";
    private final static String STATE_IS_ADD_TAG_AND_EXIT_TAGS_EDITION_MODE_BUTTONS_CONTAINER_VISIBLE = "STATE_IS_ADD_TAG_AND_EXIT_TAGS_EDITION_MODE_BUTTONS_CONTAINER_VISIBLE";
    private final static String STATE_IS_CANCEL_CONFIRM_TAG_CREATION_BUTTONS_CONTAINER_VISIBLE = "STATE_IS_CANCEL_CONFIRM_TAG_CREATION_BUTTONS_CONTAINER_VISIBLE";

    private final static int ACTIVITY_TAG_MEDIA = 0;

    private final static int REQUEST_CAPTURE_ACTIVITY = 1;
    private final static int REQUEST_TAG_MEDIA_ACTIVITY = 2;

    private InsideViewerFragment mViewer;

    private LinearLayout mEnterTagsEditionModeButtonContainer;
    private LinearLayout mAddTagAndExitTagsEditionModeButtonsContainer;
    private LinearLayout mCancelConfirmTagCreationButtonsContainer;

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Lifecycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate layout:
        setContentView(R.layout.activity_custom_inside_viewer);

        // Enable up navigation:
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Get the widgets:
        mEnterTagsEditionModeButtonContainer = (LinearLayout) findViewById(R.id.enterTagsEditionModeButtonContainer);
        mAddTagAndExitTagsEditionModeButtonsContainer = (LinearLayout) findViewById(R.id.addTagAndExitTagsEditionModeButtonsContainer);
        mCancelConfirmTagCreationButtonsContainer = (LinearLayout) findViewById(R.id.cancelConfirmButtonsContainer);
        Button enterTagsEditionModeButton = (Button) findViewById(R.id.enterTagsEditionModeButton);

        // Add the viewer:
        if (savedInstanceState == null) {

            // Get the intent data:
            Spin spin = getIntent().getParcelableExtra(EXTRA_SPIN);

            Bundle bundle = new Bundle();
            bundle.putParcelable(InsideViewerFragment.EXTRA_SPIN, spin);

            mViewer = new InsideViewerFragment();
            mViewer.setArguments(bundle);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.insideViewerContainer, mViewer, "INSIDE_VIEWER").commit();
        }
        else {

            mViewer = (InsideViewerFragment) getSupportFragmentManager().findFragmentByTag("INSIDE_VIEWER");
        }

        mViewer.setLoadingListener(this);
        mViewer.setListener(this);

        //noinspection ConstantConditions
        enterTagsEditionModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onEnterTagsEditionModeButtonClick();
            }
        });

        //noinspection ConstantConditions
        findViewById(R.id.exitTagsEditionModeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onExitTagsEditionModeButtonClick();
            }
        });

        //noinspection ConstantConditions
        findViewById(R.id.addTagButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onAddTagButtonClick();
            }
        });

        //noinspection ConstantConditions
        findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onCancelButtonClick();
            }
        });

        //noinspection ConstantConditions
        findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onConfirmButtonClick();
            }
        });

        //noinspection ConstantConditions
        findViewById(R.id.outsideViewerButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putBoolean(STATE_IS_ENTER_TAGS_EDITION_MODE_BUTTON_CONTAINER_VISIBLE, mEnterTagsEditionModeButtonContainer.getVisibility() == View.VISIBLE);
        outState.putBoolean(STATE_IS_ADD_TAG_AND_EXIT_TAGS_EDITION_MODE_BUTTONS_CONTAINER_VISIBLE, mAddTagAndExitTagsEditionModeButtonsContainer.getVisibility() == View.VISIBLE);
        outState.putBoolean(STATE_IS_CANCEL_CONFIRM_TAG_CREATION_BUTTONS_CONTAINER_VISIBLE, mCancelConfirmTagCreationButtonsContainer.getVisibility() == View.VISIBLE);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);

        boolean isEnterTagsEditionModeButtonContainerVisible = savedInstanceState.getBoolean(STATE_IS_ENTER_TAGS_EDITION_MODE_BUTTON_CONTAINER_VISIBLE);
        boolean isAddTagAndExitTagsEditionModeButtonsContainerVisible = savedInstanceState.getBoolean(STATE_IS_ADD_TAG_AND_EXIT_TAGS_EDITION_MODE_BUTTONS_CONTAINER_VISIBLE);
        boolean isCancelConfirmTagCreationButtonsContainerVisible = savedInstanceState.getBoolean(STATE_IS_CANCEL_CONFIRM_TAG_CREATION_BUTTONS_CONTAINER_VISIBLE);

        mEnterTagsEditionModeButtonContainer.setVisibility(isEnterTagsEditionModeButtonContainerVisible ? View.VISIBLE : View.GONE);
        mAddTagAndExitTagsEditionModeButtonsContainer.setVisibility(isAddTagAndExitTagsEditionModeButtonsContainerVisible ? View.VISIBLE : View.GONE);
        mCancelConfirmTagCreationButtonsContainer.setVisibility(isCancelConfirmTagCreationButtonsContainerVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case REQUEST_CAPTURE_ACTIVITY: {

                if(resultCode == InsideCaptureSourceChooserActivity.RESULT_MUST_RELOAD_INSIDE_CAPTURE) {
                    mViewer.reloadInsideCapture();
                }

                break;
            }

            case REQUEST_TAG_MEDIA_ACTIVITY: {
                mViewer.reloadSpinFromMetadata();
                break;
            }
        }
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu:
        getMenuInflater().inflate(R.menu.osc_camera_viewer_activity_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();

        if (id == android.R.id.home) { onBackPressed(); return true; }
        if (id == R.id.osc_camera_viewer_activity_menu_action_recapture) { onRecaptureButtonClick(); return true; }
        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region User interaction

    @Override
    public void onBackPressed() {

        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void onEnterTagsEditionModeButtonClick() {

        mEnterTagsEditionModeButtonContainer.setVisibility(View.GONE);
        mAddTagAndExitTagsEditionModeButtonsContainer.setVisibility(View.VISIBLE);
        mCancelConfirmTagCreationButtonsContainer.setVisibility(View.GONE);

        mViewer.setTagsEditionModeActive(true);
    }

    private void onExitTagsEditionModeButtonClick() {

        mEnterTagsEditionModeButtonContainer.setVisibility(View.VISIBLE);
        mAddTagAndExitTagsEditionModeButtonsContainer.setVisibility(View.GONE);
        mCancelConfirmTagCreationButtonsContainer.setVisibility(View.GONE);

        mViewer.setTagsEditionModeActive(false);
    }

    private void onConfirmButtonClick() {

        // Update the views visibility:
        mEnterTagsEditionModeButtonContainer.setVisibility(View.GONE);
        mAddTagAndExitTagsEditionModeButtonsContainer.setVisibility(View.VISIBLE);
        mCancelConfirmTagCreationButtonsContainer.setVisibility(View.GONE);

        if(mViewer.isMovingTag()) {
            mViewer.confirmTagPositionChange();
        }
        else {
            mViewer.confirmNewTagCreation();
        }
    }

    private void onCancelButtonClick() {

        // Update the views visibility:
        mEnterTagsEditionModeButtonContainer.setVisibility(View.GONE);
        mAddTagAndExitTagsEditionModeButtonsContainer.setVisibility(View.VISIBLE);
        mCancelConfirmTagCreationButtonsContainer.setVisibility(View.GONE);

        if(mViewer.isMovingTag()) {
            mViewer.cancelTagPositionChange();
        }
        else {
            mViewer.cancelNewTagCreation();
        }
    }

    private void onAddTagButtonClick() {

        // Update the views visibility:
        mEnterTagsEditionModeButtonContainer.setVisibility(View.GONE);
        mAddTagAndExitTagsEditionModeButtonsContainer.setVisibility(View.GONE);
        mCancelConfirmTagCreationButtonsContainer.setVisibility(View.VISIBLE);

        mViewer.startAddingNewTag();
    }

    private void onRecaptureButtonClick() {

        Intent intent = new Intent(this, InsideCaptureSourceChooserActivity.class);
        intent.putExtra(InsideCaptureSourceChooserActivity.EXTRA_FILE_NAME, mViewer.getSpin().getId());
        intent.putExtra(InsideCaptureSourceChooserActivity.EXTRA_SPIN_FOLDER, mViewer.getSpin().getFolder(this));
        startActivityForResult(intent, REQUEST_CAPTURE_ACTIVITY);
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region InsideViewerFragment.InsideViewerFragmentLoadingListener

    @Override
    public void onLoadingDone() {

        mViewer.getTagView().setListener(this);
    }

    @Override
    public void onLoadingError(InsideViewerFragment.LoadingError error) {

        Log.e(this, "An error occurred while loading the inside capture: " + error.name());
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region InsideViewerFragment.InsideViewerFragmentListener

    @Override
    public void onBackgroundClick(float x, float y) {
    }

    @Override
    public void onBackgroundClickConfirmed(float x, float y) {
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region TagView.TagViewListener

    @Override
    public void onContentViewClicked(Tag tag) {

        if(!mViewer.getTagView().isInEditMode()) {

            Intent intent = new Intent(this, TagMediaActivity.class);
            intent.putExtra(TagMediaActivity.EXTRA_SPIN, mViewer.getSpin());
            intent.putExtra(TagMediaActivity.EXTRA_TAG, tag);
            intent.putExtra(TagMediaActivity.EXTRA_CAN_EDIT_MEDIA, true);
            intent.putExtra(TagMediaActivity.EXTRA_START_MODE, TagMediaActivity.StartMode.ANIMATE_TO_IMMERSIVE.name());

            startActivityForResult(intent, ACTIVITY_TAG_MEDIA);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    @Override
    public void onEditTagButtonClicked(final Tag tag) {

        final EditText input = new EditText(this);
        input.setText(tag.getTitle());

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Edit the tag");
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                tag.setTitle(input.getText().toString());
                mViewer.updateTag(tag);
            }
        });

        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    @Override
    public void onDeleteTagButtonClicked(final Tag tag) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Warning");
        builder.setTitle("Do you really want to delete this tag?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                mViewer.deleteTag(tag);
            }
        });

        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    @Override
    public void onHandleTagMediaButtonClicked(Tag tag) {

        Intent intent = new Intent(this, TagMediaActivity.class);
        intent.putExtra(TagMediaActivity.EXTRA_SPIN, mViewer.getSpin());
        intent.putExtra(TagMediaActivity.EXTRA_TAG, tag);
        intent.putExtra(TagMediaActivity.EXTRA_CAN_EDIT_MEDIA, true);
        intent.putExtra(TagMediaActivity.EXTRA_START_MODE, TagMediaActivity.StartMode.ANIMATE_TO_IMMERSIVE.name());

        startActivityForResult(intent, REQUEST_TAG_MEDIA_ACTIVITY);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void onMoveTagButtonClicked(Tag tag) {

        mEnterTagsEditionModeButtonContainer.setVisibility(View.GONE);
        mAddTagAndExitTagsEditionModeButtonsContainer.setVisibility(View.GONE);
        mCancelConfirmTagCreationButtonsContainer.setVisibility(View.VISIBLE);

        mViewer.startChangingTagPosition(tag);
    }

    // endregion
    // ---------------------------------------------------------------------------------------------
}
