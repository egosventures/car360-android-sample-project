package com.egosventures.car360sample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.egosventures.car360sample.R;
import com.egosventures.stable360.capture.activities.CameraActivity;
import com.egosventures.stable360.capture.activities.LocalViewerActivity;
import com.egosventures.stable360.capture.fragments.LocalViewerFragment;
import com.egosventures.stable360.capture.model.LocalSpin;
import com.egosventures.stable360.capture.services.UploadService;
import com.egosventures.stable360.capture.utils.helpers.SpinManager;
import com.egosventures.stable360.core.Stable360Framework;
import com.egosventures.stable360.core.activities.Stable360BaseActivity;
import com.egosventures.stable360.core.model.CarIdentification;
import com.egosventures.stable360.core.model.Spin;
import com.egosventures.stable360.core.utils.Utils;
import com.egosventures.stable360.display.activities.OnlineViewerActivity;
import com.egosventures.stable360.display.activities.OnlineViewerConfigurationActivity;
import com.egosventures.stable360.display.fragments.OnlineViewerFragment;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    // ---------------------------------------------------------------------------------------------
    // region Attributes

    private static final String TAG = "MainActivity";

    private static final int REQUEST_LOCAL_VIEWER = 10;

    private static final int REQUEST_ONLINE_VIEWER_1 = 11;
    private static final int REQUEST_ONLINE_VIEWER_2 = 12;
    private static final int REQUEST_ONLINE_VIEWER_3 = 13;

    private static final int REQUEST_CAPTURE = 14;

    private ListView mSpinsListView;
    private List<String> mSpinFoldersList = new ArrayList<>();

    /**
     * BroadcastReceiver that will handle Upload/Rendering updates.
     */
    private BroadcastReceiver mUploadEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // Get the intent data:
            UploadService.UploadEvent event = (UploadService.UploadEvent) intent.getSerializableExtra(UploadService.EXTRA_UPLOAD_EVENT);

            switch (event) {

                case NULL_USER: {

                    Log.e(TAG, "Login to the Stable360 framework failed");
                    break;
                }

                case FRAMEWORK_INIT_ERROR: {

                    Stable360Framework.Stable360FrameworkError error = (Stable360Framework.Stable360FrameworkError) intent.getSerializableExtra(UploadService.EXTRA_FRAMEWORK_INIT_ERROR);

                    Log.e(TAG, "Stable360 framework initialisation failed: " + error.name());
                    break;
                }

                case ADDED_TO_QUEUE:
                case SPIN_UPLOAD_STARTED:
                case IMAGE_FOLDER_RESERVED:
                case FILES_TO_ARCHIVE_SELECTED:
                case ARCHIVE_CREATED:
                case ARCHIVE_UPLOAD_PROGRESSED:
                case ARCHIVE_UPLOAD_DONE: {

                    String spinId = intent.getStringExtra(UploadService.EXTRA_SPIN_ID);
                    float spinUploadProgress = intent.getFloatExtra(UploadService.EXTRA_SPIN_UPLOAD_PROGRESS, 0f);
                    float totalProgress = intent.getFloatExtra(UploadService.EXTRA_UPLOAD_PROGRESS, 0f);

                    Log.i(TAG, "Spin " + spinId + " upload progressed (" + event.name() + "): " + (int)(spinUploadProgress*100) + "% (total progress: " + (int)(totalProgress*100) + "%)");

                    // Update UI to show upload progress...

                    break;
                }
                case SPIN_UPLOAD_DONE: {

                    String spinId = intent.getStringExtra(UploadService.EXTRA_SPIN_ID);
                    String spinCode = intent.getStringExtra(UploadService.EXTRA_SPIN_CODE);

                    Log.i(TAG, "Spin " + spinId + " upload done. Spin code: " + spinCode);

                    break;
                }

                case SPIN_UPLOAD_FAILED: {

                    String spinId = intent.getStringExtra(UploadService.EXTRA_SPIN_ID);
                    UploadService.UploadError error = (UploadService.UploadError) intent.getSerializableExtra(UploadService.EXTRA_UPLOAD_ERROR);

                    Log.i(TAG, "Spin " + spinId + " upload failed. Error: " + error.name());

                    break;
                }

                case UPLOAD_DONE: {

                    Log.i(TAG, "All spins in queue have been uploaded");

                    break;
                }

                case UPLOAD_CANCELLED: {

                    Log.i(TAG, "Upload has been cancelled");

                    break;
                }
            }
        }
    };

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Activity lifecycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Call super:
        super.onCreate(savedInstanceState);

        // Inflate the layout:
        setContentView(R.layout.activity_main);

        mSpinsListView = (ListView) findViewById(R.id.listView);

        mSpinsListView.setAdapter(new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, mSpinFoldersList));

        mSpinsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String spinId = mSpinFoldersList.get(position);
                onSpinClick(spinId);
            }
        });

        updateSpinsList();

        // Register for upload notifications
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(mUploadEventReceiver, new IntentFilter(UploadService.BROADCAST_UPLOAD_EVENT));
    }

    @Override
    protected void onDestroy() {

        // Unregister from upload notifications since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mUploadEventReceiver);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(mSpinsListView != null) {

            this.updateSpinsList();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // All Stable360 activities can return RESULT_NULL_USER:
        if(resultCode == Stable360BaseActivity.RESULT_NULL_USER) {

            Log.e(TAG, "Login to the Stable360 framework failed");
        }

        // All Stable360 activities can return RESULT_FRAMEWORK_INIT_ERROR:
        else if(resultCode == Stable360BaseActivity.RESULT_FRAMEWORK_INIT_ERROR) {

            Stable360Framework.Stable360FrameworkError error = (Stable360Framework.Stable360FrameworkError) data.getSerializableExtra(Stable360BaseActivity.EXTRA_FRAMEWORK_INIT_ERROR);
            Log.e(TAG, "Stable360 framework initialisation failed: " + error.name());
        }

        // Handle specific activities results:
        else {

            switch (requestCode) {

                case REQUEST_LOCAL_VIEWER: {

                    switch (resultCode) {

                        case LocalViewerActivity.RESULT_LOADING_ERROR: {

                            LocalViewerFragment.LoadingError error = (LocalViewerFragment.LoadingError) data.getSerializableExtra(OnlineViewerActivity.EXTRA_LOADING_ERROR);

                            Log.e(TAG, "An error occurred while loading the local spin: " + error.name());

                            break;
                        }
                    }

                    break;
                }

                case REQUEST_ONLINE_VIEWER_1:
                case REQUEST_ONLINE_VIEWER_2:
                case REQUEST_ONLINE_VIEWER_3: {

                    switch (resultCode) {

                        case OnlineViewerActivity.RESULT_LOADING_ERROR: {

                            OnlineViewerFragment.LoadingError error = (OnlineViewerFragment.LoadingError) data.getSerializableExtra(OnlineViewerActivity.EXTRA_LOADING_ERROR);

                            Log.e(TAG, "An error occurred while loading the remote spin: " + error.name());

                            break;
                        }
                    }

                    break;
                }

                case REQUEST_CAPTURE: {

                    switch (resultCode) {

                        default:
                        case RESULT_OK: {

                            // Get the spin id:
                            String spinId = data.getStringExtra(CameraActivity.EXTRA_SPIN_ID);

                            Log.i(TAG, "Spin capture done. Spin id: " + spinId);
                            break;
                        }

                        case CameraActivity.RESULT_INSUFFICIENT_DISK_SPACE: {
                            Log.e(TAG, "Unable to take a new spin, insufficient disk space");
                            break;
                        }

                        case CameraActivity.RESULT_IO_ERROR: {
                            Log.e(TAG, "Unable to take a new spin, an error occurred while creating the needed folders");
                            break;
                        }

                        case CameraActivity.RESULT_NOT_ENOUGH_FRAMES: {
                            Log.e(TAG, "The spin creation failed, not enough frames have been captured");
                            break;
                        }
                    }

                    break;
                }
            }
        }
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu :
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action buttons
        int id = item.getItemId();

        if(id == R.id.main_activity_menu_action_take_new_spin) { onTakeNewSpinButtonClick(); return true; }
        else if(id == R.id.main_activity_menu_action_open_online_spin) { onOpenOnlineSpinButtonClick(); return true; }

        else { return super.onOptionsItemSelected(item); }
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region User interaction

    private void onSpinClick(final String spinId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("What do you want to do?");
        builder.setItems(new CharSequence[] {
                "Open the spin in the default viewer",
                "Open the spin in the custom viewer",
                "Open the spin in the simple custom viewer (with no tags edition support)",
                "Upload the spin",
                "Delete the spin"
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                switch (i) {

                    case 0: {
                        openDefaultLocalViewer(spinId);
                        break;
                    }
                    case 1: {
                        openCustomLocalViewer(spinId);
                        break;
                    }
                    case 2: {
                        openSimpleCustomLocalViewer(spinId);
                        break;
                    }
                    case 3: {
                        uploadSpin(spinId);
                        break;
                    }
                    case 4: {
                        deleteSpin(spinId);
                        updateSpinsList();
                        break;
                    }
                }
            }
        });
        builder.show();
    }

    public void onTakeNewSpinButtonClick() {
        this.takeNewSpin();
    }

    public void onOpenOnlineSpinButtonClick() {

        Intent intent = new Intent(this, OnlineViewerConfigurationActivity.class);
        startActivity(intent);
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Utils

    /**
     * Opening of the simple custom local viewer activity (with no tags edition support), using the
     * LocalViewerFragment
     */
    public void openSimpleCustomLocalViewer(String spinId) {

        Intent viewerIntent = new Intent(this, SimpleCustomLocalViewerActivity.class);
        viewerIntent.putExtra(SimpleCustomLocalViewerActivity.EXTRA_SPIN_ID, spinId);
        startActivityForResult(viewerIntent, REQUEST_LOCAL_VIEWER);
    }

    /**
     * Opening of the custom local viewer activity, using the LocalViewerFragment
     */
    public void openCustomLocalViewer(String spinId) {

        Intent viewerIntent = new Intent(this, CustomLocalViewerActivity.class);
        viewerIntent.putExtra(CustomLocalViewerActivity.EXTRA_SPIN_ID, spinId);
        startActivityForResult(viewerIntent, REQUEST_LOCAL_VIEWER);
    }

    /**
     * To open the local viewer:
     *
     * 1. Create a new intent with LocalViewerActivity.class
     * 2. Required: call putExtra with the spinId to open
     * 3. Optional: create a list of buttons to show in the viewer, and pass them to the intent
     * 4. Call "startActivity" with the created intent
     */
    public void openDefaultLocalViewer(String spinId) {

        Intent viewerIntent = new Intent(this, LocalViewerActivity.class);
        viewerIntent.putExtra(LocalViewerActivity.EXTRA_SPIN_ID, spinId);

        // add needed buttons
        ArrayList<LocalViewerActivity.MenuButton> buttons = new ArrayList<>();
        buttons.add(LocalViewerActivity.MenuButton.DELETE);
        buttons.add(LocalViewerActivity.MenuButton.UPLOAD);
        buttons.add(LocalViewerActivity.MenuButton.THUMBNAILS);
        buttons.add(LocalViewerActivity.MenuButton.TAGS);

        viewerIntent.putExtra(LocalViewerActivity.EXTRA_MENU_ITEMS, buttons);
        viewerIntent.putExtra(LocalViewerActivity.EXTRA_BACKGROUND_COLOR, Color.BLACK);
        viewerIntent.putExtra(LocalViewerActivity.EXTRA_MENU_ITEMS, buttons);
        viewerIntent.putExtra(LocalViewerActivity.EXTRA_USE_INSIDE_VIEWER, true);
        viewerIntent.putExtra(LocalViewerActivity.EXTRA_USE_TAGS, true);

        startActivityForResult(viewerIntent, REQUEST_LOCAL_VIEWER);
    }

    /**
     * To open the CameraActivity:
     *
     * 1. Create a new intent with CameraActivity.class
     * 2. Optional: call putExtra with a `CarIdentification` object containing the identification
     *    for the car (VIN, LICENCE or CUSTOM).
     *    If not set or if called with "CUSTOM", an id will be auto-generated
     * 3. Call "startActivity" with the created intent
     */
    public void takeNewSpin() {

        Intent cameraIntent = new Intent(this, CameraActivity.class);
        CarIdentification carIdentification = new CarIdentification("TESTVIN_" + new Date().getTime(), CarIdentification.CarIdentificationType.VIN);
        cameraIntent.putExtra(CameraActivity.EXTRA_CAR_IDENTIFICATION, carIdentification);
        cameraIntent.putExtra(CameraActivity.EXTRA_CAR_CONDITION, Spin.CarCondition.USED);
        cameraIntent.putExtra(CameraActivity.EXTRA_CAPTURE_TYPE, Spin.CaptureType.WALK_AROUND);
        cameraIntent.putExtra(CameraActivity.EXTRA_SHOW_CAMERA_SETTINGS, true);

        startActivityForResult(cameraIntent, REQUEST_CAPTURE);
    }

    /**
     * To retrieve the local spins:
     *
     * List the folders located in `Utils.getLocalSpinsFolder`.
     */
    public void updateSpinsList() {

        File[] spinFolders = Utils.getLocalSpinsFolder(this).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return dir.isDirectory();
            }
        });

        this.mSpinFoldersList = new ArrayList<>();

        if(spinFolders != null) {

            Log.i(TAG, "spinFolders size: " + spinFolders.length);

            for (File spinFolder : spinFolders) {
                this.mSpinFoldersList.add(spinFolder.getName());
            }
        }
        else {

            Log.i(TAG, "spinFolders size: " + null);
        }

        Collections.sort(this.mSpinFoldersList);

        ArrayAdapter adapter = (ArrayAdapter) this.mSpinsListView.getAdapter();
        adapter.clear();
        adapter.addAll(this.mSpinFoldersList);
        adapter.notifyDataSetChanged();
    }

    /**
     * To start the rendering/upload of a spin:
     *
     * 1. Optional: if you want to receive notifications on the upload status, create a
     * `BroadcastReceiver`that handles the upload event (see below)
     *        1a. Register for upload notifications in the "OnCreate" method
     *        1b. Don't forget to unregister when the activity is destroyed (onDestroy)
     * 2. Call the "SpinManager.startRenderingSpin" method to start the upload.
     */
    public void uploadSpin(String spinId) {
        SpinManager.uploadSpin(this, LocalSpin.withId(this, spinId));
    }

    /**
     * To delete a spin, just call the SpinManager.deleteSpin method
     */
    public void deleteSpin(String spinId) {
        SpinManager.deleteSpin(this, spinId);
    }

    // endregion
    // ---------------------------------------------------------------------------------------------
}
